﻿using System;
using System.IO;
using UniAppBuildHelper.PartCode;

namespace UniAppBuildHelper
{
    class Program
    {
        static void Main(string[] args)
        {
            beginToConvert();
            Console.WriteLine($"生成完成~~~，请查看：{Path.Combine(Directory.GetCurrentDirectory(), "dist")}");
            foreach (var directory in Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), "dist")))
            {
                Console.WriteLine(directory);
            }
        }

        public static void CreateNewFile(dynamic obj, string fileName)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "dist");
            Directory.CreateDirectory(path);
            System.IO.File.WriteAllText(Path.Combine(path,fileName), obj.TransformText());
        }

        public static void beginToConvert()
        {
            string packageName = BaseConfig.GetValue("packageName");
            string APPID = BaseConfig.GetValue("APPID");
            string AppName = BaseConfig.GetValue("AppName");
            if (string.IsNullOrEmpty(packageName) || string.IsNullOrEmpty(APPID))
            {
                throw new ArgumentNullException("packageName或APPID不存在,请传入");
            }

            #region AndroidManifest.xml
            AndroidManifest aManifest = new AndroidManifest(packageName);
            CreateNewFile(aManifest, "AndroidManifest.xml");
            #endregion

            #region WXEntryActivity.java
            WXEntryActivity wxEntryActivity = new WXEntryActivity(packageName);
            CreateNewFile(wxEntryActivity, "WXEntryActivity.java");
            #endregion

            #region WXPayEntryActivity.java
            WXPayEntryActivity wxPayEntryActivity = new WXPayEntryActivity(packageName);
            CreateNewFile(wxPayEntryActivity, "WXPayEntryActivity.java");
            #endregion

            #region dcloud_control.xml
            dcloud_control dcloudControl = new dcloud_control(packageName);
            CreateNewFile(dcloudControl, "dcloud_control.xml");
            #endregion

            #region build.gradle
            buildGradle build = new buildGradle(packageName);
            CreateNewFile(build, "build.gradle");
            #endregion

            #region strings
            stringsXml setAppNameXml = new stringsXml(AppName);
            CreateNewFile(setAppNameXml, "strings.xml");
            #endregion
        }
    }
}
