﻿namespace UniAppBuildHelper.PartCode
{
    partial class dcloud_control
    {
        private readonly string packageName;
        private readonly string APPID;

        public dcloud_control(string packageName)
        {
            this.packageName = packageName;
            this.APPID = BaseConfig.GetValue("APPID");
        }
    }
}
