﻿namespace UniAppBuildHelper.PartCode
{
    partial class AndroidManifest
    {
        private string PackageName { get; set; }
        public AndroidManifest(string _packageName)
        {
            PackageName = _packageName;
        }
    }
}
