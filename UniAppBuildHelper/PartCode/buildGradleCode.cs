﻿namespace UniAppBuildHelper.PartCode
{
    partial class buildGradle
    {
        private readonly string packageName;
        private readonly string cer_pwd;
        private readonly string cer_alias;
        private readonly string cer_alias_pwd;
        private readonly string cer_path;

        public buildGradle(string packageName)
        {
            this.packageName = packageName;
            this.cer_pwd = BaseConfig.GetValue("CerPwd");
            this.cer_alias = BaseConfig.GetValue("CerAlias");
            this.cer_alias_pwd = BaseConfig.GetValue("CerAliasPwd");
            this.cer_path = BaseConfig.GetValue("CerPath");
        }
    }
}
