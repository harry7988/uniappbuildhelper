﻿using Microsoft.Extensions.Configuration;

namespace UniAppBuildHelper
{
    public static class BaseConfig
    {
        public static string GetValue(string key)
        {
            IConfigurationRoot config = new ConfigurationBuilder().AddEnvironmentVariables().Build();
            return config[key];
        }

    }
}
